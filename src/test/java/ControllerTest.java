import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;


class ControllerTest {
    private OurInterface ourInterface = Mockito.spy(new OurInterfaceImpl());
    private Controller test = new Controller(ourInterface);

    @Test
    public void testElse() {
        test.foo(100);
        Mockito.verify(ourInterface, Mockito.times(2)).foo(100);
    }
}