
public class Controller {
    private OurInterface ourInterface;

    public Controller(OurInterface ourInterface) {
        this.ourInterface = ourInterface;
    }

    public void foo(int value) {
        if (value == 10) {
            boolean ddd = true;
        } else if (value == 11) {
            ourInterface.methodFirst(11);
            ourInterface.methodFirst(11);
        } else {
            ourInterface.foo(value);
            ourInterface.foo(value);
            ourInterface.foo(value + 3);
            ourInterface.foo(value + 2);
            ourInterface.foo(value + 1);
        }
    }

}
